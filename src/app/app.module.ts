import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	MatTableModule,
	MatCheckboxModule,
	MatButtonModule,
	MatFormFieldModule,
	MatInputModule,
	MatRippleModule,
	MatSelectModule,
	MatSidenavModule,
	MatCardModule,
	MatToolbarModule,
	MatSliderModule,
	MatSnackBarModule,
	MatDialogModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DialogOverviewComponent } from './dialog-overview/dialog-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogOverviewComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatCardModule,
    MatToolbarModule,
    MatSliderModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  exports: [
  	MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatCardModule,
    MatToolbarModule,
    MatSliderModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  entryComponents: [
	DialogOverviewComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
