import { Component, ViewChild, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroup } from '@angular/forms';
import { MatTable } from '@angular/material';
import {MatDialog} from '@angular/material/dialog';

import { DialogOverviewComponent } from './dialog-overview/dialog-overview.component';

export interface PeriodicElement {
  cedula: number;
  name: string;
  nivel: number;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {cedula: 1425435325, name: 'Arturo', nivel: 1, description: 'Lorem ipsum dolor'},
  {cedula: 143556676587, name: 'Carlos', nivel: 3, description: 'Lorem ipsum dolor'},
  {cedula: 190896754, name: 'Edgar', nivel: 4, description: 'Lorem ipsum dolor'},
  {cedula: 134567, name: 'Maria', nivel: 5, description: 'Lorem ipsum dolor'},
  {cedula: 143556676587, name: 'Carlos', nivel: 3, description: 'Lorem ipsum armas'},
  {cedula: 1985, name: 'Jose', nivel: 0, description: 'Lorem ipsum dolor'},
  {cedula: 190896754, name: 'Artuiro', nivel: 4, description: 'Lorem ipsum dolor'},
  {cedula: 24354351, name: 'Diana', nivel: 2, description: 'Lorem sexo dolor'},
  {cedula: 134567, name: 'Laura', nivel: 5, description: 'Lorem ipsum dolor'},
  {cedula: 7895761, name: 'Jorge', nivel: 4, description: 'Lorem ipsum dolor'},
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'incidencias-app';
	displayedColumns: string[] = ['cedula', 'name', 'nivel', 'description'];
	badKeywords: string[] = ['sexo', 'droga', 'arma'];
	regSource = ELEMENT_DATA;
	@ViewChild(MatTable,{static:true}) table: MatTable<any>;

	registerForm = new FormGroup({
	  cedulaControl : new FormControl(''),
	  nameControl : new FormControl(''),
	  nivelControl : new FormControl(0),
	  descriptionControl : new FormControl('')
	});

  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog) {}
  addReg(){
  	console.log(this.regSource);
  	if (this.registerForm.status == "VALID") {
  		this.regSource.unshift({cedula: parseInt(this.registerForm.value.cedulaControl), 
  							  name: this.registerForm.value.nameControl,
  							  nivel: parseInt(this.registerForm.value.nivelControl),
  							  description: this.registerForm.value.descriptionControl});
  		this.openSnackBar("Registro guardado correctamente", "OK");
  		this.table.renderRows();
  		this.registerForm.reset();
  	}
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  getTotalByProm(){
  	let total = 0;
  	for (var i = this.regSource.length - 1; i >= 0; i--) {
  		total += this.regSource[i].nivel;
  	}
  	let prom = total/this.regSource.length;
  	let filterNivel = [];
	filterNivel['>'] = this.regSource.filter(function(element, index, array) {
		return (element.nivel >= prom);
	});
	filterNivel['<'] = this.regSource.filter(function(element, index, array) {
		return (element.nivel <= prom);
	});
	//order by nivel
	let filterNivelSort = [];
	filterNivelSort['>'] = filterNivel['>'].sort((a, b) => (a.nivel > b.nivel) ? 1 : -1);
	filterNivelSort['<'] = filterNivel['<'].sort((a, b) => (a.nivel > b.nivel) ? -1 : 1);

	let finalData = filterNivelSort['>'].slice(0,3);
	finalData = finalData.concat(filterNivelSort['<'].slice(0,3));
  	console.log(filterNivelSort['>'].slice(0,3),filterNivelSort['<'].slice(0,3),filterNivelSort);
  	this.openDialog(prom, finalData);
  }

  openDialog(prom, finalData): void {
    const dialogRef = this.dialog.open(DialogOverviewComponent, {
      width: '550px',
      data: {title: 'Promedio nivel de incidencia: '+prom, finalData: finalData}
    });
  }

  checKeywords(): void{
  	let rgReport = [];

	for (var j = this.regSource.length - 1; j >= 0; j--) {
		let reported = false;
	  	for (var i = this.badKeywords.length - 1; i >= 0; i--) {
			var str = (this.regSource[j].description).toLowerCase();
	  		if (str.search(this.badKeywords[i]) != -1 ) { 
			   reported=true;
			}
	  	}
		if (reported) { 
		   rgReport.push(this.regSource[j]);
		}
	}

	console.log(rgReport);

    const dialogRef = this.dialog.open(DialogOverviewComponent, {
      width: '550px',
      data: {title: 'Incidenetes con las palabras: '+this.badKeywords.join(', '), finalData: rgReport}
    });
  }

  groupByName(): void{
  	let grouped = {};
  	for (var j = this.regSource.length - 1; j >= 0; j--) {
  		if (grouped[this.regSource[j].name] === undefined) {
  			grouped[this.regSource[j].name] = [];
  		}
  		grouped[this.regSource[j].name].push(this.regSource[j]);
  	}
  	let max = [];
  	for (let key of Object.keys(grouped)) {
	  let item = grouped[key];
	  if (max.length<item.length) {
  			max=item;
  		}
	}
  	console.log(grouped,max);

    const dialogRef = this.dialog.open(DialogOverviewComponent, {
      width: '550px',
      data: {title: 'Persona con mas incidentes: '+max[0].name+' con ('+max.length+')', finalData: max}
    });
  }
}
